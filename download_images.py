import os
import requests

def download_images(schema,
parent_folder,
n_images_per_location=None):
    '''
    tree structure:
    images:
        <location>:
            <place_md5>_<photo_filename>
            <review_md5>_<photo_filename>
    '''
    for location in schema:
        location_md5 = location['md5']
        location_desc = location['description']
        for ix,image_url in enumerate(location_desc['desc']['photos_urls']):
            if n_images_per_location:
                if ix >= n_images_per_location:
                    break 
            #TODO: add downloading for reviews
            process_fname_before_saving = lambda fname,parent_folder=parent_folder,location_md5=location_md5: os.path.join(parent_folder,f'{location_md5}_{fname}')
            download_image(image_url,process_fname_before_saving)
            pass

    pass
def download_image(image_url,process_fname_before_saving):
    #========================================
    url_parts = (image_url).split('/')
    fname = url_parts[-1]
    new_fname = process_fname_before_saving(fname)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    response = requests.get(image_url)
    file = open(new_fname, "wb")
    file.write(response.content)
    file.close()
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # os.rename(fname,new_fname)
    pass

def setup(parent_folder,purge=True):
    if purge:
        os.system(f'rm -rf {parent_folder}')
    try:
        os.mkdir(parent_folder)
    except FileExistsError:
        pass
    pass

if __name__ == '__main__':
    parent_folder = args.parent_folder
    schema_json = 'json_fname'
    schema = read_schema(schema_json)
    setup(parent_folder)
    download_images(schema,
    parent_folder,
    n_images_per_location=None)

def test_download_images():
    from download_images import (setup,download_images)
    parent_folder = 'images'
    schema = [{
        'md5':'india_md5',
        'description':{
            'desc':{
        'photos_urls':['https://www.google.com/images/branding/googlelogo/1x/googlelogo_white_background_color_272x92dp.png']
        }
            }}]

    setup(parent_folder)
    download_images(schema,
    parent_folder,
    n_images_per_location=None)

if __name__ == '__main__':
    test_download_images()
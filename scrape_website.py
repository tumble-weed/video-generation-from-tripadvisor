#!/usr/bin/python
# from msilib.schema import File
import sys, os
import argparse
import time
import json
from urllib.parse import urljoin, urlparse
from hashlib import md5
from selenium import webdriver
from selenium.webdriver.support.relative_locator import locate_with
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from fake_useragent import UserAgent
from pprint import pprint
from selenium.common.exceptions import WebDriverException, NoSuchElementException, ElementNotInteractableException, TimeoutException, StaleElementReferenceException

site_base = "https://www.tripadvisor.in/"
#site_seed = ["https://www.tripadvisor.in/Attraction_Review-g304551-d20291559-Reviews-Weekend_Getaways_From_Delhi-New_Delhi_National_Capital_Territory_of_Delhi.html", 
#site_seed = ["https://www.tripadvisor.in/Attraction_Review-g304551-d3879008-Reviews-Food_Tour_in_Delhi-New_Delhi_National_Capital_Territory_of_Delhi.html"] 
# site_seed = ["https://www.tripadvisor.in/Tourism-g297665-Rajasthan-Vacations.html"]
site_seed = ['https://www.tripadvisor.in/Hotel_Review-g319729-d21392701-Reviews-Nature_Village_Resort-Pushkar_Ajmer_District_Rajasthan.html']#NOTE:problematic site for capturing photos urls
site_tag = "tripadvisor"


def get_links(driver, url) : 

    linkList = []
    llinkElements = []
    linkElements = driver.find_elements(By.XPATH, "//a[@href]")

    for element in linkElements : 
        try : 
            link = element.get_attribute("href")
        except StaleElementReferenceException : 
            pass

        if link is None :
            continue

        uparse = urlparse(url)
        if site_tag not in uparse[1] or link.startswith('#'): 
            continue 
        elif not uparse[1] or link.startswith('/') :  
            link = urljoin(url, link)

        linkList.append(link)

    return linkList

    #try :
    #    #if not isinstance(url, unicode) : 
        #   url = url.encode('utf-8')
#=================================================================================
def format_reviews(reviews):
    return reviews
    pass
def save_data(reviews,fname):
    pass

def get_page_data(driver,url) : 

    try : 
        driver.get(url)
    except : 
        return {}, []

    url_list = get_links(driver, url)
    
    POI = ['AttractionProductReview']
    #if not urlparse(url)[-1].split('-')[0] in POI : 
    #    return {}, url_list

    reviews = get_reviews(driver, url)
    
    if not reviews : 
        return {}, url_list
    desc = get_desc(driver, url)
    
    if reviews or desc : 
        #========================================================
        #NOTE: to debug photos not being captured, Aniket
        # stops whenever a review has text but not
        if len(reviews) > 0:
            stop_flags = []
            
            for i in range(len(reviews)):
                has_content = len(reviews[i]['content']) > 0
                has_photos = len(reviews[i]['photos_urls']) > 0
                has_content_no_photos = ( not has_photos) and has_content
                stop_flags.append(has_content_no_photos)
            if any(stop_flags):
                with open('dump.html','w') as f:
                    f.write(driver.page_source)
                import pdb;pdb.set_trace()
        #========================================================

        data_out = {'md5':md5(url.encode('utf-8')).hexdigest(), 
                    'desc':desc, 
                    'reviews':reviews}
        print("REVIEWS : {}\nPHOTOS : {}\n".format(len(reviews), len(desc["photos_urls"])))

        return data_out, url_list
    
    return {}, url_list

def get_reviews(driver,base_url,start_ix = 0):
    '''
    generates urls for the next set of 10 reviews
    till no valid reviews
    '''
    #TODO: some reviews have read more. do we want to deal with that?
    if len(base_url.split('Review')) < 2 : 
        # Only URLs with Reviews word in them.
        return []

    reviews = []
    chunk_ix = start_ix
    MAX_CHUNK_IX = 3

    review_selector = 'div#tab-data-qa-reviews-0, div[data-test-target="reviews-tab"], div.was-ssr-only > div:nth-child(3)'
    next_xpath = '//div[@id="tab-data-qa-reviews-0"]/div/div[last()]/div[last()]//a[@aria-label="Next page"]'

    while(chunk_ix < MAX_CHUNK_IX):

        if chunk_ix>0:

            chunk = chunk_ix*10
            try : 
                next_button = WebDriverWait(driver, 10).until(ec.element_to_be_clickable((By.XPATH, next_xpath)))
            except TimeoutException :
                break
    
            if next_button : 
                driver.execute_script("arguments[0].click()", next_button)
                time.sleep(3)
            else : 
                break

        else:
            url = base_url

        # Get Reviews from URL
        try : 
            review_result = WebDriverWait(driver, 10).until(ec.presence_of_element_located((By.CSS_SELECTOR,review_selector)))
            review_result = review_extraction(review_result)
        except (TimeoutException) as exc: 
            print(exc)
            review_result = []

        if not review_result : 
            break
        reviews += review_result
        chunk_ix += 1

    return reviews

def get_desc(driver, url) :
    ''' Get all photos urlList as part of description. 
    #TODO Get description where it is available
    '''
    MAX_PHOTO_LIMIT = 100
    desc = {'text' : '', 'photos_urls' : []}
    photos_urls = []
    try : 
        next_button = WebDriverWait(driver, 5).until(ec.element_to_be_clickable((By.CSS_SELECTOR, 'button[aria-label="Next Photo"]')))
    except (NoSuchElementException, TimeoutException) : 
        return desc

    photo_selector = 'div > div > a > ul > li > div'
    #div > div > div:nth-child(3) > div:nth-child(5) > div.bQAKy.f.z > div > div > img
    next_count = 0 

    while(next_count < MAX_PHOTO_LIMIT) :
        
        try : 
            new_urls = driver.find_elements(By.CSS_SELECTOR, photo_selector)

            photos_urls += [r.get_attribute('style').lstrip('background-image: url("').rstrip('");') for r in new_urls]
            try : 
                next_button = WebDriverWait(driver, 5).until(ec.element_to_be_clickable((By.CSS_SELECTOR, 'button[aria-label="Next Photo"]')))
            except (NoSuchElementException, TimeoutException) : 
                break
            next_button.click()
            next_count += 1
        except (NoSuchElementException, ElementNotInteractableException) : 
            break 

    photos_urls = list(set(photos_urls))
    desc = {'text' : '', 'photos_urls' : photos_urls}
    return desc 

def review_extraction(source) : 
    ''' Get review with review selector '''
    review_selector = ['']*3
    review_selector[0] = "//div[@id='tab-data-qa-reviews-0']/div/div[last()]/div/span/span"
    review_selector[1] = "//div[@class='was-ssr-only']/div[3]/div/div[last()]/div/span/span"
    review_selector[2] = "//div[@data-test-target='reviews-tab']//*[@data-reviewid]"
    reviews = []
    curr_selector = ''
    for si, selector in enumerate(review_selector) :  
        reviews = source.find_elements(By.XPATH, selector)
        
        #print(selector, len(reviews))
        if reviews : 
            curr_selector = si
            break
    
    # remove unsplittable rogue comments with disclaimer put in by tripadvisor
    reviews_out = []
    for r in reviews : 
        if curr_selector == 2 : 
            # based on https://stackoverflow.com/questions/62600288/how-to-handle-lazy-loaded-images-in-selenium
            SCROLL_PAUSE_TIME = 0.5
            # driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            # time.sleep(SCROLL_PAUSE_TIME)
            title = r.find_element(By.XPATH, "div[2]")
            body = r.find_element(By.XPATH, "div/div[1]/div[1]/q/span")
            # Problematic extraction of review images for selector 2
            
            preceding_divs = r.find_elements(By.XPATH, "preceding-sibling::div")
            for element in preceding_divs:
                # stackoverflow scroll to element
                driver.execute_script("arguments[0].scrollIntoView();", element)
                time.sleep(SCROLL_PAUSE_TIME)

            photos = r.find_elements(By.XPATH, "preceding-sibling::div//img[@src]")
        else : 
            title = r.find_element(By.XPATH, "div[4]")
            body = r.find_element(By.XPATH, "div[6]")
            #NOTE: this is untested, i just copied the functionality above, Aniket
            to_scroll = r.find_elements(By.XPATH, "div/button")
            for element in to_scroll:
                # stackoverflow scroll to element
                driver.execute_script("arguments[0].scrollIntoView();", element)
                time.sleep(SCROLL_PAUSE_TIME)
            photos = r.find_elements(By.XPATH, "div/button/picture/img")
        #======================================================
        # #NOTE: for debugging Aniket
        # if 'It was awsome staying there ..' in body.text:
        #     import pdb;pdb.set_trace()
        #======================================================
        if title :
            title = title.text
        else : 
            title = ""
        if body : 
            body = body.text 
        else : 
            body = ""
        #TODO: remove the default-avatar image
        if photos : 
            # Check here if the extracted photos have the required attribute
            photos = [p.get_attribute('src') for p in photos] 
        else : 
            photos = []
        if not (title or body or photos) :
            continue
        reviews_out.append({'md5': md5(title.encode('utf-8')+body.encode('utf-8')).hexdigest(), 
                        'title':title, 
                        'content':body, 
                        'photos_urls':photos})

    # only description and titles should remain, and they alternate
    # create md5 of (review title + body )
    return reviews_out




def setup(
    log_folder,output_folder,
    fname_scraped_log,fname_not_scraped_log,
    head
):
    #======================================
    if not os.path.isdir(log_folder) :
        os.system('mkdir {}'.format(log_folder))
    #======================================
    if not os.path.isdir(output_folder) :
        os.system('mkdir {}'.format(output_folder))
    #======================================
    fname_scraped_log = os.path.join(log_folder, fname_scraped_log)
    fname_not_scraped_log = os.path.join(log_folder, fname_not_scraped_log)
    if not os.path.isfile(fname_scraped_log):
        os.system('touch {}'.format(fname_scraped_log))
        pass
    if not os.path.isfile(fname_not_scraped_log):
        os.system('touch {}'.format(fname_not_scraped_log))
        pass
    #========================================
    options = Options()
    options.add_argument("window-size=1400,600")
    ua = UserAgent()
    user_agent = ua.random
    options.add_argument(f'user-agent={user_agent}')
    if not head:
        options.add_argument('--headless')
    driver = webdriver.Chrome(options=options)

    #========================================
    return fname_scraped_log,fname_not_scraped_log,driver

def crawl(
    fname_scraped_log,
    fname_not_scraped_log,
    driver,
    args):

    crawled_list = []
    scraped_log =  open(fname_scraped_log, 'r+')
    not_scraped_log = open(fname_not_scraped_log, 'r+')

    driver.get(site_base)

    
    scraped_list = [x.strip() for x in scraped_log.read().split('\n')]
    not_scraped_list = [x.strip() for x in not_scraped_log.read().split('\n')]  
    scraped_counter = 0
    crawled_counter = 0

    url_list = set(site_seed)

    print("Scraped Log : {}\nNot_scraped_log : {}".format(scraped_log.name, not_scraped_log.name))

    while crawled_counter < 100 : 
        scraped_log.flush()
        not_scraped_log.flush()
        url = url_list.pop()
        url = url.split('#')[0]
        if url in site_seed or (url not in crawled_list and url not in scraped_list and url not in not_scraped_list) : 
            print("URL : {}".format(url))
            page_data, temp_url_list = get_page_data(driver,url)
            
            crawled_list.append(url)
            crawled_counter += 1
            url_list = url_list | set(temp_url_list)
            if page_data : 
                with open(os.path.join(args.output_folder, page_data['md5']+'.json'), 'w') as fout : 
                    json.dump(page_data, fout)
                scraped_counter += 1
                scraped_log.write(url + '\n')
                scraped_list.append(url)
            else : 
                not_scraped_log.write(url + '\n')
                not_scraped_list.append(url)
            

    print(len(crawled_list))
    scraped_log.close()
    not_scraped_log.close()
    driver.close()


#=======================================
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--log-folder',type=str,required=True)
    parser.add_argument('--output-folder',type=str,required=True)
    parser.add_argument('--fname-scraped-log',type=str,default='scraped.log')
    parser.add_argument('--fname-not-scraped-log',type=str,default='not_scraped.log')
    parser.add_argument('--head',action='store_true',default=False)
    args = parser.parse_args()
    return args

if __name__ == '__main__' : 
    #TODO: deal with sign-in page https://www.tripadvisor.in/RegistrationController?flow=core_combined&pid=40187&returnTo=%2FInbox&fullscreen=true
    args = get_args()
    #=======================================
    log_folder = args.log_folder
    output_folder = args.output_folder
    fname_scraped_log = args.fname_scraped_log
    fname_not_scraped_log = args.fname_not_scraped_log
    head = args.head
    #=======================================
    fname_scraped_log,fname_not_scraped_log,driver = setup(
    log_folder,output_folder,
    fname_scraped_log,fname_not_scraped_log,head
    )
    #=======================================
    crawl(fname_scraped_log,fname_not_scraped_log,driver, args)

import skimage.io
import skimage.transform
import numpy as np
import os
"""
test suite for video creation
"""

def test_single_frame_video():
    from video import create_video #TODO
    #im_fname = #TODO
    im_fname
    pass
'''
def test_pan():
    start_center_yx = #TODO
    end_center_yx = #TODO
    pass
'''
def test_pan_zoom():
    from video import create_frame
    from video import get_transforms
    im_fname = 'cat singing.jpg'
    im = skimage.io.imread(im_fname)
    frame_size = (100,100)

    start_y = 0.3
    end_y  = 0.5

    start_x = 0.5
    end_x  = 0.5

    
    start_zoom = 0.5
    end_zoom  = 1
    n_transforms = 10

    zooms,cys,cxs = get_transforms(
    start_zoom,end_zoom,
    start_y,end_y,
    start_x,end_x,
    n_transforms)


    save_dir = 'test_pan_zoom_frames'
    try:
        os.system(f'rm -rf {save_dir}')
    except FileExistsError:
        pass
    os.mkdir(save_dir)
    for i,(cy,cx,zoom) in enumerate(zip(cys,cxs,zooms)):
        panned = create_frame(im,frame_size,zoom,(cy,cx))
        skimage.io.imsave(os.path.join(save_dir,f'{i}.png'),panned)
    pass


def test_pan():
    from video import create_frame
    im_fname = 'cat singing.jpg'
    im = skimage.io.imread(im_fname)
    im = skimage.transform.resize(im,(100,100))
    start_y = 0.3
    end_y  = 0.5

    start_x = 0.5
    end_x  = 0.5
    
    zoom = 1
    n_pans = 2
    frame_size = (100,100)
    save_dir = 'test_pan_frames'
    try:
        os.system(f'rm -rf {save_dir}')
    except FileExistsError:
        pass
    os.mkdir(save_dir)
    for i,(cy,cx) in enumerate(zip( 
        np.linspace(start_y,end_y,n_pans),
        np.linspace(start_x,end_x,n_pans)
    )
        ):
        panned = create_frame(im,frame_size,zoom,(cy,cx))
        skimage.io.imsave(os.path.join(save_dir,f'{i}.png'),panned)
    pass

def test_zoom():
    from video import create_frame
    im_fname = 'cat singing.jpg'
    im = skimage.io.imread(im_fname)
    
    start_zoom = 0.5#TODO
    end_zoom  = 1#TODO
    n_zooms = 2
    frame_size = (100,100)
    center_yx = (0.5,0.5)
    save_dir = 'test_zoom_frames'
    try:
        os.system(f'rm -rf {save_dir}')
    except FileExistsError:
        pass
    os.mkdir(save_dir)
    for i,zoom in enumerate(np.linspace(start_zoom,end_zoom,n_zooms)):
        zoomed = create_frame(im,frame_size,zoom,center_yx)
        skimage.io.imsave(os.path.join(save_dir,f'{i}.png'),zoomed)
    pass
if __name__ == '__main__':
    #TODO 'single_frame_video'
    #DONE 'test_zoom','test_pan'
    cases = ['test_pan_zoom']
    if 'single_frame_video' in cases:
        test_single_frame()
        pass
    elif 'test_zoom' in cases:
        test_zoom()
        pass
    elif 'test_pan' in cases:
        test_pan()
        pass    
    elif 'test_pan_zoom' in cases:
        test_pan_zoom()
        pass        
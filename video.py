"""
utilities and functions for creating a vide with
pand and zoom
"""
import skimage.transform
import numpy as np
def crop(im,center_yx,crop_size):
    inH,inW = im.shape[:2]
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    frame_out = np.zeros(crop_size+(3,))
    oH,oW = crop_size
    cy,cx = center_yx
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    top_overflow = cy-(oH//2)
    left_overflow = cx-(oW//2)
    bottom_overflow = cy+(oH//2)
    right_overflow = cx+(oW//2)
    cropped = im[max(top_overflow,0):min(bottom_overflow,inH),max(left_overflow,0):min(right_overflow,inW)]#TODO(zoomed,center_yx,frame_size)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    
    start_y = max(0,-top_overflow) # if zoomed ran out above, top_overflow would be -ve
    start_x = max(0,-left_overflow)  # if zoomed ran out left, left_overflow would be -ve
    end_y = oH - max(bottom_overflow - inH,0)  # if zoomed ran out bottom, right_overflow would be larger than inH
    end_x = oW - max(right_overflow - inW,0)  # if zoomed ran out right, right_overflow would be larger than inW
    frame_out[start_y:end_y,start_x:end_x] = cropped
    return frame_out

    pass
def create_frame(high_res,frame_size,zoom,center_yx_float):
    zoomed = skimage.transform.rescale(high_res,zoom,channel_axis=-1)
    zH,zW = zoomed.shape[:2]
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cy,cx = zH*center_yx_float[0],zW*center_yx_float[1]
    cy,cx = int(cy),int(cx)
    # outH,outW = zoomed.shape[:2]
    cropped = crop(zoomed,(cy,cx),frame_size)
    return cropped
def get_transforms(
    start_zoom,end_zoom,
    start_y,end_y,
    start_x,end_x,
    n_transforms):
    zooms = np.linspace(start_zoom,end_zoom,n_transforms)
    cy = np.linspace(start_y,end_y,n_transforms)
    cx = np.linspace(start_x,end_x,n_transforms)
    #TODO: ensure that cy,cx such that the crop doesnt go out of frame...
    return zooms,cy,cx

    
# def get_dump_frames(frames_dir):
# 	def dump_frames(frame):
# 		#TODO
# 	return dump_frames
# def create_video_from_frames(frames):
# 	pass
# def create_video_from_saved_frames(frames):
# 	pass
# def get_transforms(high_res,frame_size,initial_zoom,final_zoom,initial_center_yx,final_center_yx):
# 	pass

# # frames_dir = #TODO
# # dump_frames = get_dump_frames(frames_dir)
# # transformations = get_transforms()
# # apply(orignal_image,n_frames,params,dump_frames)

# # def apply(orignal_image,n_frames,params,stash_frames):
# # 	fix =0
	
# # 	while fix < n_frames:
# # 		frame = create_frame(params[fix])
# # 		stash_frames(frame)
# # 	pass